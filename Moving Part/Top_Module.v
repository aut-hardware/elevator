`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:03:08 12/26/2018 
// Design Name: 
// Module Name:    Top_Module 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Top_Module(clk,  reset, InReq,  Present,lock,Eng, Door,Alarm);

input clk;
input reset;
input [3:1] InReq;
//input [3:1] OutReq;
input [3:1] Present;
input lock;
output [1:0] Eng; 
output [3:1] Door;
output Alarm;

wire clk_int;
elevator DUT(clk_int,  reset, InReq, InReq, Present,lock,Eng, Door,Alarm);
freq_divider(.in_freq(clk), .out_freq(clk_int), .reset(reset));


endmodule
