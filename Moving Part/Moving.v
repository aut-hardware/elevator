`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:59:47 12/23/2018 
// Design Name: 
// Module Name:    Moving 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module elevator(CLK,  Reset, InReq,OutReq,  Present,lock,Eng, Door,Alarm);
	 
	 
	// input lock;
	 input CLK;
    input Reset;
    input [3:1] InReq;
    input [3:1] OutReq;
    input [3:1] Present;
	 input lock;
	 output [1:0] Eng;
    output [3:1] Door;
	 output Alarm;
	 
	 parameter [3:0] T1=0, T2up=1, T2down=2, T3=3,
							Up=4, Down=5,
							Delay1=6, Delay2=7,
							Check13=8, Check2up=9, Check2down=10,
							Alarm1=11, Alarm2=12;
	
	 reg Alarm;
    reg [1:0] Eng;
    reg [3:1] Door;

   
							

	reg [3:0] current;  
	reg [3:0] DelayCounter = 4'b0000;
	reg [1:0] LockCounter = 2'b00;
	reg [2:0] AlarmCounter = 3'b000;
	reg AlarmValue = 1'b0;
	reg DelayValue = 1'b0;
	


	wire [3:1] req;
	
	
	or or1(req[1],InReq[1],OutReq[1]),
	   or2(req[2],InReq[2],OutReq[2]),
	   or3(req[3],InReq[3],OutReq[3]);
		
	
		
		
		
/*
	always @ (InReq)
		begin
			if(InReq[1])
			   req1[1] = ~ req1[1];
			if(InReq[2])
				req1[2] = ~ req1[2];
			if(InReq[3])
				req1[3] = ~ req1[3];
		end
	always @ (OutReq)
		begin
			if(OutReq[1])
				req2[1] = ~ req2[1];
			if(OutReq[2])
				req2[2] = ~ req2[2];
			if(OutReq[3])
				req2[3] = ~ req2[3];
		end
*/

/*
	always @ (posedge CLK)
		if( Reset) current <= T1;

		else current <= current;
*/

	always @ (posedge CLK)
			if(Reset) current <= T1;
			else 
				case(current)
					T1:
						begin
							Alarm = 1'b0;
							Eng=2'b00;
							Door=3'b001;
						
							AlarmValue=0;
							if (LockCounter == 2'b11) 
								begin 
									AlarmValue=1;
									current <= Alarm1;
									LockCounter = 0;
								end
							
							else if(req[2] | req[3] )	current <= Check13;
							else current <= T1;
						end
					T2up:
						begin
							Alarm = 1'b0;					
							Eng=2'b00;
							Door=3'b010;
						
							AlarmValue=0;
							if (LockCounter == 2'b11) 
								begin 
									AlarmValue=1;
									current <= Alarm1;
									LockCounter = 0;
								end
							else if(req[3] | req[1]) current <= Check2up;
							else current <= T2up;
						end
					T2down:
						begin
							Alarm = 1'b0;			
							Eng=2'b00;
							Door=3'b010;
						
							AlarmValue=0;
							if (LockCounter == 2'b11) 
								begin 
									AlarmValue=1;
									current <= Alarm2;
									LockCounter = 0;
								end
							else if(req[1] | req[3]) current <= Check2down;
							else current <= T2down;
						end
					T3:
						begin	
							Alarm = 1'b0;

								Eng=2'b00;
								Door=3'b100;					
					
							AlarmValue=0;
							DelayValue=0;
							DelayCounter = 4'b0000;
							if (LockCounter == 2'b11) 
								begin 
									AlarmValue=1;
									current <= Alarm1;
									LockCounter = 0;
								end
							else if((req[2] | req[1] ))	current <= Check13;
							else current <= T3;
						end
					Up:
						begin
							Alarm = 1'b0;
							Eng = 2'b10;
							Door = 3'b000;
							LockCounter = 0;
							if((req[2] & Present[2]) | (~req[3] & Present[2] )) current <= T2down ; 
							else if ( Present[3]) current <= T3;
							else current <= Up;
						end
					Down:
						begin
							Alarm = 1'b0;
							Eng = 2'b11;
							Door = 3'b000;
							LockCounter = 0;
							if((req[2] & Present[2]) | (~req[1] & Present[2] )) current <= T2up ; 
							else if ( Present[1]) current <= T1;
							else current <= Down;
						end
					Check13:
						begin
							Alarm = 1'b0;
							Eng = 2'b00;
							if(Present==3'b100)
								Door = 3'b100;
							else
								Door = 3'b001;
							if(lock) 
								begin
									DelayValue=1;
									DelayCounter = 4'b0000;
									LockCounter = LockCounter + 1'b1;
									current <= Delay1;
								end
							else if(Present[3]) current <= Down;
							else current <= Up;
						
						end
					Check2up:
						begin
							Alarm = 1'b0;
							Eng = 2'b00;
							Door = 3'b010;
							if(lock) 
							begin
								DelayValue=1;
								DelayCounter = 0;
								LockCounter = LockCounter + 1'b1;
								current <= Delay1;
							end
						else if(req[1]) current <= Down;
						else current <= Up;
						end
					Check2down:
						begin
							Alarm = 1'b0;
							Eng = 2'b00;
							Door = 3'b010;
						if(lock) 
							begin
								DelayValue=1;
								DelayCounter = 0;
								LockCounter = LockCounter + 1'b1;
								current <= Delay2;
							end
						else if(req[3]) current <= Up;
							else current <= Down;
						end
					Delay1:
						begin
							Alarm = 1'b0;
							Eng = 2'b00;
							if(Present[1]) Door = 3'b001;
							else if(Present[2]) Door = 3'b010;
							else	
								begin
									Door = 3'b100;
								end
								
							if(DelayCounter == 4'b1010 | DelayValue==0)
								begin
									if(AlarmValue) current <= Alarm1;
									else
										begin
											DelayValue=0;
											if(Present[1]) current <= T1;
											else if (Present[2]) current <= T2up;
											else if (Present[3]) current <= T3;
										end
								end
							else 
								begin
									DelayCounter = DelayCounter + 4'b0001;
									current <= Delay1;
								end
						end
					Delay2:
						begin
							Alarm = 1'b0;
							Eng = 2'b00;
							Door = 3'b010;
							if(DelayCounter == 4'b1010  | DelayValue==0)
								if (AlarmValue)current <= Alarm2;
								else 
									begin
										current<=T2down;
										DelayValue = 0;
									end
							else 
								begin
									DelayCounter = DelayCounter + 1'b1;
									current <= Delay2;
								end	
						end
					Alarm1:
						begin
							Alarm = 1'b1;
							Eng = 2'b00;
							if(Present[1]) Door = 3'b001;
							else if(Present[2]) Door = 3'b010;
							else 	Door = 3'b100;
							if(AlarmCounter==3'b101)
								if(Present[1]) current <= T1;
								else if (Present[2]) current <= T2up;
								else current <= T3;
							else
								begin
									AlarmCounter= AlarmCounter+ 1'b1;
									current <= Alarm1;
								end
						end
					Alarm2:
						begin
							Alarm = 1'b1;
							Eng = 2'b00;
							Door= 3'b010;
							if(AlarmCounter== 3'b101)
								current <= T2down;
							else
								begin
									AlarmCounter = AlarmCounter + 1'b1;
									current <= Alarm2;
								end
						end
			endcase
		/*always @ (current)
			begin
			/*	if(AlarmValue)
					Alarm=1;
				else	
					Alarm=0;
				case(current)
					T1:
						begin
							Eng=2'b00;
							Door=3'b001;
						end
					T2up:
						begin
							Eng=2'b00;
							Door=3'b010;
						end
					
					T2down:
						begin
							Eng=2'b00;
							Door=3'b010;
						end
					
					T3:
						begin
							Eng=2'b00;
							Door=3'b100;
						end
					Up:
						begin
							Eng = 2'b10;
							Door = 3'b000;
						end
					Down:
						begin
							Eng = 2'b11;
							Door = 3'b000;
						end
					Check13:
						begin
							Eng = 2'b00;
							if(Present==3'b100)
								Door = 3'b100;
							else
								Door = 3'b001;
						end
					Check2up:
						begin
							Eng = 2'b00;
							Door = 3'b010;
						end
					Check2down:
						begin
							Eng = 2'b00;
							Door = 3'b010;
						end
					Delay1:
						begin
							Eng = 2'b00;
							Door = 3'b111; // ):
						end
					Delay2:
						begin
							Eng = 2'b00;
							Door = Door; // ):
						end
					
						
				endcase
			
			end*/
endmodule
