
# PlanAhead Launch Script for Post-Synthesis pin planning, created by Project Navigator

create_project -name LC_Project -dir "C:/Users/DataZip/Desktop/LC_Project/LC_Project/planAhead_run_3" -part xc3s400pq208-5
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "C:/Users/DataZip/Desktop/LC_Project/LC_Project/Top_Module.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {C:/Users/DataZip/Desktop/LC_Project/LC_Project} }
set_param project.pinAheadLayout  yes
set_property target_constrs_file "C:/Users/DataZip/Desktop/New folder (10)/LD_PRJ-testcases/LD_PRJ-testcases/port.ucf" [current_fileset -constrset]
add_files [list {C:/Users/DataZip/Desktop/New folder (10)/LD_PRJ-testcases/LD_PRJ-testcases/port.ucf}] -fileset [get_property constrset [current_run]]
link_design
