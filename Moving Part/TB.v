`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    00:14:32 12/24/2018 
// Design Name: 
// Module Name:    TB 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module TB(
    );

    reg CLK;
    reg Reset;
    reg [3:1] InReq;
    reg [3:1] OutReq;
    reg [3:1] Present;
	 reg lock;
	 
	 wire Alarm;
    wire [1:0] Eng;
    //reg [1:0] Eng;
    wire [3:1] Door;
    //reg [3:1] Door;
	 
	 Moving Duty (CLK,Reset,InReq,OutReq,Present,lock,Alarm,Eng,Door);
	//Moving Duty (CLK,Reset,InReq,OutReq,Present,lock,Eng,Door);
	 
	 initial
		begin
			CLK=0;
			repeat(200)
				begin
					#10;
					CLK = ~CLK;
				end
		end
		
		
	initial
		begin
			Present = 3'b001;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=0;
			Reset=0;
			#20
			
			Present = 3'b001;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=0;
			Reset=1;
			#20
			
			Present = 3'b001;
			InReq = 3'b100;
			OutReq = 3'b000;
			lock=0;
			Reset=1;
			#20
			
			Present = 3'b001;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=0;
			Reset=1;
			#20;
			
			Present = 3'b000;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=0;
			Reset=1;
			#20;
			
			Present = 3'b000;
			InReq = 3'b010;
			OutReq = 3'b000;
			lock=0;
			Reset=1;
			#20;
			
			Present = 3'b010;
			InReq = 3'b001;
			OutReq = 3'b000;
			lock=0;
			Reset=1;			
			#20;
			
			Present = 3'b010;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=0;
			Reset=1;		
			#20;
			
			Present = 3'b000;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=0;
			Reset=1;
			#20;
			
			Present = 3'b100;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=0;
			Reset=1;
			#20;
			
			Present = 3'b100;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=0;
			Reset=1;
			#20;
						
			Present = 3'b100;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=0;
			Reset=1;
			#20;
			
			Present = 3'b000;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=0;
			Reset=1;
			#20;
			
			Present = 3'b000;
			InReq = 3'b101;
			OutReq = 3'b000;
			lock=0;
			Reset=1;
			#20;
			
			Present = 3'b010;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=0;
			Reset=1;
			#20;
			
			Present = 3'b010;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=0;
			Reset=1;
			#20;
			
			Present = 3'b000;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=0;
			Reset=1;
			#20;
			
			Present = 3'b100;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=0;
			Reset=1;
			#20;
			
			Present = 3'b100;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=0;
			Reset=1;
			#20;
			
			Present = 3'b100;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=0;
			Reset=1;
			#20;
			
			//Phase 1 completed
			/*
			
			Present = 3'b100;
			InReq = 3'b000;
			OutReq = 3'b001;
			lock=1;
			Reset=1;
			#20;
			
			Present = 3'b100;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=0;
			Reset=1;
			#280;
			
			Present = 3'b100;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=0;
			Reset=1;
			#40;
			
			Present = 3'b000;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=0;
			Reset=1;
			#20;
			
			Present = 3'b010;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=0;
			Reset=1;
			#20;
			
			Present = 3'b000;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=0;
			Reset=1;
			#20;
			
			Present = 3'b001;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=0;
			Reset=1;
			#20;*/
			/*
			Present = 3'b001;
			InReq = 3'b000;
			OutReq = 3'b010;
			lock=1;
			Reset=1;
			#20;
			
			Present = 3'b001;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=0;
			Reset=1;
			#200;
			
			Present = 3'b001;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=1;
			Reset=1;
			#20;
			
			Present = 3'b001;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=0;
			Reset=1;
			#200;
			
			Present = 3'b001;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=1;
			Reset=1;
			#20;
			
			Present = 3'b001;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=0;
			Reset=1;
			#200
			
			Present = 3'b001;
			InReq = 3'b000;
			OutReq = 3'b100;
			lock=0;
			Reset=1;
			#20;

			Present = 3'b001;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=0;
			Reset=1;
			#20;
			
			Present = 3'b000;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=0;
			Reset=1;
			#20;
			
			Present = 3'b010;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=0;
			Reset=1;
			#20;
			
			Present = 3'b010;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=0;
			Reset=1;
			#20;
			
			Present = 3'b000;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=0;
			Reset=1;
			#20;
			
			Present = 3'b100;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=0;
			Reset=1;
			#20;
			
			Present = 3'b100;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=0;
			Reset=1;
			#20;
			
			Present = 3'b100;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=0;
			Reset=1;
			#20;
			
			Present = 3'b100;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=0;
			Reset=1;
			#20;
			
			Present = 3'b100;
			InReq = 3'b000;
			OutReq = 3'b000;
			lock=0;
			Reset=1;
			#20;
			*/
			
		end
	 
	 
endmodule
