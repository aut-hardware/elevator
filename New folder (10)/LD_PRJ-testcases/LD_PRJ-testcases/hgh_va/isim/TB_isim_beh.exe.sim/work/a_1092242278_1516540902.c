/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/vahid/Desktop/LD_PRJ/elevator.vhd";
extern char *IEEE_P_2592010699;

char *ieee_p_2592010699_sub_1735675855_503743352(char *, char *, char *, char *, char *, char *);
unsigned char ieee_p_2592010699_sub_1744673427_503743352(char *, char *, unsigned int , unsigned int );
unsigned char ieee_p_2592010699_sub_2545490612_503743352(char *, unsigned char , unsigned char );


static void work_a_1092242278_1516540902_p_0(char *t0)
{
    char t1[16];
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    unsigned int t8;
    unsigned int t9;
    unsigned char t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;

LAB0:    xsi_set_current_line(29, ng0);

LAB3:    t2 = (t0 + 1352U);
    t3 = *((char **)t2);
    t2 = (t0 + 7412U);
    t4 = (t0 + 1512U);
    t5 = *((char **)t4);
    t4 = (t0 + 7428U);
    t6 = ieee_p_2592010699_sub_1735675855_503743352(IEEE_P_2592010699, t1, t3, t2, t5, t4);
    t7 = (t1 + 12U);
    t8 = *((unsigned int *)t7);
    t9 = (1U * t8);
    t10 = (3U != t9);
    if (t10 == 1)
        goto LAB5;

LAB6:    t11 = (t0 + 4824);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    t14 = (t13 + 56U);
    t15 = *((char **)t14);
    memcpy(t15, t6, 3U);
    xsi_driver_first_trans_fast(t11);

LAB2:    t16 = (t0 + 4696);
    *((int *)t16) = 1;

LAB1:    return;
LAB4:    goto LAB2;

LAB5:    xsi_size_not_matching(3U, t9, 0);
    goto LAB6;

}

static void work_a_1092242278_1516540902_p_1(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    unsigned char t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;

LAB0:    xsi_set_current_line(32, ng0);
    t1 = (t0 + 1192U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB2;

LAB4:    xsi_set_current_line(35, ng0);
    t1 = (t0 + 992U);
    t3 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t1, 0U, 0U);
    if (t3 != 0)
        goto LAB5;

LAB7:
LAB6:
LAB3:    t1 = (t0 + 4712);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(33, ng0);
    t1 = (t0 + 4888);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);
    goto LAB3;

LAB5:    xsi_set_current_line(36, ng0);
    t2 = (t0 + 2312U);
    t5 = *((char **)t2);
    t4 = *((unsigned char *)t5);
    t2 = (t0 + 4888);
    t6 = (t2 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = t4;
    xsi_driver_first_trans_fast(t2);
    goto LAB6;

}

static void work_a_1092242278_1516540902_p_2(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    char *t4;
    char *t5;
    int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned int t9;
    unsigned char t10;
    char *t11;
    char *t12;
    int t13;
    unsigned int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned char t17;
    unsigned char t18;
    unsigned char t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    static char *nl0[] = {&&LAB4, &&LAB7, &&LAB3, &&LAB5, &&LAB6, &&LAB8};

LAB0:    xsi_set_current_line(43, ng0);
    t1 = (t0 + 2152U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (char *)((nl0) + t3);
    goto **((char **)t1);

LAB2:    t1 = (t0 + 4728);
    *((int *)t1) = 1;

LAB1:    return;
LAB3:    xsi_set_current_line(45, ng0);
    t4 = (t0 + 2472U);
    t5 = *((char **)t4);
    t6 = (2 - 3);
    t7 = (t6 * -1);
    t8 = (1U * t7);
    t9 = (0 + t8);
    t4 = (t5 + t9);
    t10 = *((unsigned char *)t4);
    t11 = (t0 + 2472U);
    t12 = *((char **)t11);
    t13 = (3 - 3);
    t14 = (t13 * -1);
    t15 = (1U * t14);
    t16 = (0 + t15);
    t11 = (t12 + t16);
    t17 = *((unsigned char *)t11);
    t18 = ieee_p_2592010699_sub_2545490612_503743352(IEEE_P_2592010699, t10, t17);
    t19 = (t18 == (unsigned char)3);
    if (t19 != 0)
        goto LAB9;

LAB11:    xsi_set_current_line(48, ng0);
    t1 = (t0 + 4952);
    t2 = (t1 + 56U);
    t4 = *((char **)t2);
    t5 = (t4 + 56U);
    t11 = *((char **)t5);
    *((unsigned char *)t11) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);

LAB10:    goto LAB2;

LAB4:    xsi_set_current_line(51, ng0);
    t1 = (t0 + 2472U);
    t2 = *((char **)t1);
    t6 = (2 - 3);
    t7 = (t6 * -1);
    t8 = (1U * t7);
    t9 = (0 + t8);
    t1 = (t2 + t9);
    t10 = *((unsigned char *)t1);
    t17 = (t10 == (unsigned char)3);
    if (t17 == 1)
        goto LAB15;

LAB16:    t3 = (unsigned char)0;

LAB17:    if (t3 != 0)
        goto LAB12;

LAB14:    t1 = (t0 + 1672U);
    t2 = *((char **)t1);
    t6 = (3 - 3);
    t7 = (t6 * -1);
    t8 = (1U * t7);
    t9 = (0 + t8);
    t1 = (t2 + t9);
    t3 = *((unsigned char *)t1);
    t10 = (t3 == (unsigned char)3);
    if (t10 != 0)
        goto LAB18;

LAB19:    xsi_set_current_line(55, ng0);
    t1 = (t0 + 4952);
    t2 = (t1 + 56U);
    t4 = *((char **)t2);
    t5 = (t4 + 56U);
    t11 = *((char **)t5);
    *((unsigned char *)t11) = (unsigned char)0;
    xsi_driver_first_trans_fast(t1);

LAB13:    goto LAB2;

LAB5:    xsi_set_current_line(58, ng0);
    t1 = (t0 + 2472U);
    t2 = *((char **)t1);
    t6 = (3 - 3);
    t7 = (t6 * -1);
    t8 = (1U * t7);
    t9 = (0 + t8);
    t1 = (t2 + t9);
    t3 = *((unsigned char *)t1);
    t10 = (t3 == (unsigned char)3);
    if (t10 != 0)
        goto LAB20;

LAB22:    t1 = (t0 + 2472U);
    t2 = *((char **)t1);
    t6 = (3 - 3);
    t7 = (t6 * -1);
    t8 = (1U * t7);
    t9 = (0 + t8);
    t1 = (t2 + t9);
    t10 = *((unsigned char *)t1);
    t17 = (t10 == (unsigned char)2);
    if (t17 == 1)
        goto LAB25;

LAB26:    t3 = (unsigned char)0;

LAB27:    if (t3 != 0)
        goto LAB23;

LAB24:    xsi_set_current_line(62, ng0);
    t1 = (t0 + 4952);
    t2 = (t1 + 56U);
    t4 = *((char **)t2);
    t5 = (t4 + 56U);
    t11 = *((char **)t5);
    *((unsigned char *)t11) = (unsigned char)3;
    xsi_driver_first_trans_fast(t1);

LAB21:    goto LAB2;

LAB6:    xsi_set_current_line(65, ng0);
    t1 = (t0 + 2472U);
    t2 = *((char **)t1);
    t6 = (1 - 3);
    t7 = (t6 * -1);
    t8 = (1U * t7);
    t9 = (0 + t8);
    t1 = (t2 + t9);
    t3 = *((unsigned char *)t1);
    t10 = (t3 == (unsigned char)3);
    if (t10 != 0)
        goto LAB28;

LAB30:    t1 = (t0 + 2472U);
    t2 = *((char **)t1);
    t6 = (1 - 3);
    t7 = (t6 * -1);
    t8 = (1U * t7);
    t9 = (0 + t8);
    t1 = (t2 + t9);
    t10 = *((unsigned char *)t1);
    t17 = (t10 == (unsigned char)2);
    if (t17 == 1)
        goto LAB33;

LAB34:    t3 = (unsigned char)0;

LAB35:    if (t3 != 0)
        goto LAB31;

LAB32:    xsi_set_current_line(69, ng0);
    t1 = (t0 + 4952);
    t2 = (t1 + 56U);
    t4 = *((char **)t2);
    t5 = (t4 + 56U);
    t11 = *((char **)t5);
    *((unsigned char *)t11) = (unsigned char)4;
    xsi_driver_first_trans_fast(t1);

LAB29:    goto LAB2;

LAB7:    xsi_set_current_line(72, ng0);
    t1 = (t0 + 2472U);
    t2 = *((char **)t1);
    t6 = (2 - 3);
    t7 = (t6 * -1);
    t8 = (1U * t7);
    t9 = (0 + t8);
    t1 = (t2 + t9);
    t10 = *((unsigned char *)t1);
    t17 = (t10 == (unsigned char)3);
    if (t17 == 1)
        goto LAB39;

LAB40:    t3 = (unsigned char)0;

LAB41:    if (t3 != 0)
        goto LAB36;

LAB38:    t1 = (t0 + 1672U);
    t2 = *((char **)t1);
    t6 = (1 - 3);
    t7 = (t6 * -1);
    t8 = (1U * t7);
    t9 = (0 + t8);
    t1 = (t2 + t9);
    t3 = *((unsigned char *)t1);
    t10 = (t3 == (unsigned char)3);
    if (t10 != 0)
        goto LAB42;

LAB43:    xsi_set_current_line(76, ng0);
    t1 = (t0 + 4952);
    t2 = (t1 + 56U);
    t4 = *((char **)t2);
    t5 = (t4 + 56U);
    t11 = *((char **)t5);
    *((unsigned char *)t11) = (unsigned char)1;
    xsi_driver_first_trans_fast(t1);

LAB37:    goto LAB2;

LAB8:    xsi_set_current_line(79, ng0);
    t1 = (t0 + 2472U);
    t2 = *((char **)t1);
    t6 = (2 - 3);
    t7 = (t6 * -1);
    t8 = (1U * t7);
    t9 = (0 + t8);
    t1 = (t2 + t9);
    t3 = *((unsigned char *)t1);
    t4 = (t0 + 2472U);
    t5 = *((char **)t4);
    t13 = (1 - 3);
    t14 = (t13 * -1);
    t15 = (1U * t14);
    t16 = (0 + t15);
    t4 = (t5 + t16);
    t10 = *((unsigned char *)t4);
    t17 = ieee_p_2592010699_sub_2545490612_503743352(IEEE_P_2592010699, t3, t10);
    t18 = (t17 == (unsigned char)3);
    if (t18 != 0)
        goto LAB44;

LAB46:    xsi_set_current_line(81, ng0);
    t1 = (t0 + 4952);
    t2 = (t1 + 56U);
    t4 = *((char **)t2);
    t5 = (t4 + 56U);
    t11 = *((char **)t5);
    *((unsigned char *)t11) = (unsigned char)5;
    xsi_driver_first_trans_fast(t1);

LAB45:    goto LAB2;

LAB9:    xsi_set_current_line(46, ng0);
    t20 = (t0 + 4952);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    *((unsigned char *)t24) = (unsigned char)0;
    xsi_driver_first_trans_fast(t20);
    goto LAB10;

LAB12:    xsi_set_current_line(52, ng0);
    t11 = (t0 + 4952);
    t12 = (t11 + 56U);
    t20 = *((char **)t12);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    *((unsigned char *)t22) = (unsigned char)3;
    xsi_driver_first_trans_fast(t11);
    goto LAB13;

LAB15:    t4 = (t0 + 1672U);
    t5 = *((char **)t4);
    t13 = (2 - 3);
    t14 = (t13 * -1);
    t15 = (1U * t14);
    t16 = (0 + t15);
    t4 = (t5 + t16);
    t18 = *((unsigned char *)t4);
    t19 = (t18 == (unsigned char)3);
    t3 = t19;
    goto LAB17;

LAB18:    xsi_set_current_line(54, ng0);
    t4 = (t0 + 4952);
    t5 = (t4 + 56U);
    t11 = *((char **)t5);
    t12 = (t11 + 56U);
    t20 = *((char **)t12);
    *((unsigned char *)t20) = (unsigned char)5;
    xsi_driver_first_trans_fast(t4);
    goto LAB13;

LAB20:    xsi_set_current_line(59, ng0);
    t4 = (t0 + 4952);
    t5 = (t4 + 56U);
    t11 = *((char **)t5);
    t12 = (t11 + 56U);
    t20 = *((char **)t12);
    *((unsigned char *)t20) = (unsigned char)0;
    xsi_driver_first_trans_fast(t4);
    goto LAB21;

LAB23:    xsi_set_current_line(61, ng0);
    t11 = (t0 + 4952);
    t12 = (t11 + 56U);
    t20 = *((char **)t12);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    *((unsigned char *)t22) = (unsigned char)1;
    xsi_driver_first_trans_fast(t11);
    goto LAB21;

LAB25:    t4 = (t0 + 2472U);
    t5 = *((char **)t4);
    t13 = (1 - 3);
    t14 = (t13 * -1);
    t15 = (1U * t14);
    t16 = (0 + t15);
    t4 = (t5 + t16);
    t18 = *((unsigned char *)t4);
    t19 = (t18 == (unsigned char)3);
    t3 = t19;
    goto LAB27;

LAB28:    xsi_set_current_line(66, ng0);
    t4 = (t0 + 4952);
    t5 = (t4 + 56U);
    t11 = *((char **)t5);
    t12 = (t11 + 56U);
    t20 = *((char **)t12);
    *((unsigned char *)t20) = (unsigned char)1;
    xsi_driver_first_trans_fast(t4);
    goto LAB29;

LAB31:    xsi_set_current_line(68, ng0);
    t11 = (t0 + 4952);
    t12 = (t11 + 56U);
    t20 = *((char **)t12);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    *((unsigned char *)t22) = (unsigned char)0;
    xsi_driver_first_trans_fast(t11);
    goto LAB29;

LAB33:    t4 = (t0 + 2472U);
    t5 = *((char **)t4);
    t13 = (3 - 3);
    t14 = (t13 * -1);
    t15 = (1U * t14);
    t16 = (0 + t15);
    t4 = (t5 + t16);
    t18 = *((unsigned char *)t4);
    t19 = (t18 == (unsigned char)3);
    t3 = t19;
    goto LAB35;

LAB36:    xsi_set_current_line(73, ng0);
    t11 = (t0 + 4952);
    t12 = (t11 + 56U);
    t20 = *((char **)t12);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    *((unsigned char *)t22) = (unsigned char)4;
    xsi_driver_first_trans_fast(t11);
    goto LAB37;

LAB39:    t4 = (t0 + 1672U);
    t5 = *((char **)t4);
    t13 = (2 - 3);
    t14 = (t13 * -1);
    t15 = (1U * t14);
    t16 = (0 + t15);
    t4 = (t5 + t16);
    t18 = *((unsigned char *)t4);
    t19 = (t18 == (unsigned char)3);
    t3 = t19;
    goto LAB41;

LAB42:    xsi_set_current_line(75, ng0);
    t4 = (t0 + 4952);
    t5 = (t4 + 56U);
    t11 = *((char **)t5);
    t12 = (t11 + 56U);
    t20 = *((char **)t12);
    *((unsigned char *)t20) = (unsigned char)2;
    xsi_driver_first_trans_fast(t4);
    goto LAB37;

LAB44:    xsi_set_current_line(80, ng0);
    t11 = (t0 + 4952);
    t12 = (t11 + 56U);
    t20 = *((char **)t12);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    *((unsigned char *)t22) = (unsigned char)1;
    xsi_driver_first_trans_fast(t11);
    goto LAB45;

}

static void work_a_1092242278_1516540902_p_3(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    static char *nl0[] = {&&LAB6, &&LAB7, &&LAB3, &&LAB4, &&LAB4, &&LAB5};

LAB0:    xsi_set_current_line(87, ng0);
    t1 = (t0 + 2152U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (char *)((nl0) + t3);
    goto **((char **)t1);

LAB2:    t1 = (t0 + 4744);
    *((int *)t1) = 1;

LAB1:    return;
LAB3:    xsi_set_current_line(89, ng0);
    t4 = (t0 + 7532);
    t6 = (t0 + 5016);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    memcpy(t10, t4, 3U);
    xsi_driver_first_trans_fast_port(t6);
    xsi_set_current_line(90, ng0);
    t1 = (t0 + 7535);
    t4 = (t0 + 5080);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t1, 2U);
    xsi_driver_first_trans_fast_port(t4);
    goto LAB2;

LAB4:    xsi_set_current_line(92, ng0);
    t1 = (t0 + 7537);
    t4 = (t0 + 5016);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t1, 3U);
    xsi_driver_first_trans_fast_port(t4);
    xsi_set_current_line(93, ng0);
    t1 = (t0 + 7540);
    t4 = (t0 + 5080);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t1, 2U);
    xsi_driver_first_trans_fast_port(t4);
    goto LAB2;

LAB5:    xsi_set_current_line(95, ng0);
    t1 = (t0 + 7542);
    t4 = (t0 + 5016);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t1, 3U);
    xsi_driver_first_trans_fast_port(t4);
    xsi_set_current_line(96, ng0);
    t1 = (t0 + 7545);
    t4 = (t0 + 5080);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t1, 2U);
    xsi_driver_first_trans_fast_port(t4);
    goto LAB2;

LAB6:    xsi_set_current_line(98, ng0);
    t1 = (t0 + 7547);
    t4 = (t0 + 5016);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t1, 3U);
    xsi_driver_first_trans_fast_port(t4);
    xsi_set_current_line(99, ng0);
    t1 = (t0 + 7550);
    t4 = (t0 + 5080);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t1, 2U);
    xsi_driver_first_trans_fast_port(t4);
    goto LAB2;

LAB7:    xsi_set_current_line(101, ng0);
    t1 = (t0 + 7552);
    t4 = (t0 + 5016);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t1, 3U);
    xsi_driver_first_trans_fast_port(t4);
    xsi_set_current_line(102, ng0);
    t1 = (t0 + 7555);
    t4 = (t0 + 5080);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t1, 2U);
    xsi_driver_first_trans_fast_port(t4);
    goto LAB2;

}


extern void work_a_1092242278_1516540902_init()
{
	static char *pe[] = {(void *)work_a_1092242278_1516540902_p_0,(void *)work_a_1092242278_1516540902_p_1,(void *)work_a_1092242278_1516540902_p_2,(void *)work_a_1092242278_1516540902_p_3};
	xsi_register_didat("work_a_1092242278_1516540902", "isim/TB_isim_beh.exe.sim/work/a_1092242278_1516540902.didat");
	xsi_register_executes(pe);
}
