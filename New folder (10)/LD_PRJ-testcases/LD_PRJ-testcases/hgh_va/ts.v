`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date:    13:43:32 12/23/2018
// Design Name:
// Module Name:    elevator
// Project Name:
// Target Devices:
// Tool versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module elevator(Clk, Rst, in_button, out_button, presence_of_elev, motor);

    input Clk, Rst;
    input [2:0] in_button, out_button;
    input [2:0] presence_of_elev;
    output [1:0] motor;
    reg [3:0] state;
   
    parameter s1 = 4'b0001, // s1 => first floor
    s2 = 4'b0010, // s2 => second floor
    s3 = 4'b0011, // s3 => third floor
    s4 = 4'b0100, // s4 => 1 to 2
    s5 = 4'b0101, // s5 => 2 to 3
    s6 = 4'b0110, // s6 => 3 to 2
    s7 = 4'b0111, // s7 => 2 to 1
    s8 = 4'b1000, // s8 => 1 to 3
    s9 = 4'b1001; // s9 => 3 to 1
   
    always @ (posedge Clk or negedge Rst)
          if (~Rst)
                begin
                    state = s1;
                end
        else case (state)
            s1: begin
                if ((in_button == 3'b100) || (out_button == 3'b100))
                    begin
                        state = s8;
                        //motor <= 2'b10;
                    end
                else if ((in_button == 3'b010) || (out_button == 3'b010))
                    begin
                        state = s4;
                        //motor <= 2'b10;
                    end
                else if ((in_button == 3'b001) || (out_button == 3'b001))
                    begin
                        state = s1;
                    end
               end
               
            s2: begin
                if ((in_button == 3'b100) || (out_button == 3'b100))
                    begin
                        state = s5;
                        //motor <= 2'b10;
                    end
                else if ((in_button == 3'b001) || (out_button == 3'b001))
                    begin
                        state = s7;
                        //motor <= 2'b11;
                    end
                else if ((in_button == 3'b010) || (out_button == 3'b010))
                    begin
                        state = s2;
                    end
                end
               
            s3: begin
                if ((in_button == 3'b001) || (out_button == 3'b001))
                    begin
                        state = s9;
                        //motor <= 2'b11;
                    end
                else if ((in_button == 3'b010) || (out_button == 3'b010))
                    begin
                        state = s6;
                        //motor <= 2'b11;
                    end
                else if ((in_button == 3'b100) || (out_button == 3'b100))
                    begin
                        state = s3;
                    end
                end
               
            s4: begin
                state = s2;
                //motor <= 2'b10;
                end
           
            s5: begin
                state = s3;
                //motor <= 2'b10;
                end
           
            s6: begin
                state = s2;
                //motor <= 2'b11;
                end
           
            s7: begin
                state = s1;
                //motor <= 2'b11;
                end
               
            s8: begin
                if ((in_button == 3'b010) || (out_button == 3'b010))
                    begin
                        state = s6;
                        //motor <= 2'b10;
                    end
                end
               
            s9: begin
                if ((in_button == 3'b010) || (out_button == 3'b010))
                    begin
                        state = s4;
                        //motor <= 2'b11;
                    end
                end
                endcase
endmodule