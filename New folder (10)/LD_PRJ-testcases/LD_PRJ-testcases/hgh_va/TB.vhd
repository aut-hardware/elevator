
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY TB IS
END TB;
 
ARCHITECTURE behavior OF TB IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT top_module
    PORT(
         clk : IN  std_logic;
         rst : IN  std_logic;
         test_case : IN  std_logic_vector(2 downto 0);
         motor_state : OUT  std_logic_vector(1 downto 0);
         SEG_SEL : OUT  std_logic_vector(4 downto 0);
         SEG_DATA : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal rst : std_logic := '1';
   signal test_case : std_logic_vector(2 downto 0) := (others => '0');

 	--Outputs
   signal motor_state : std_logic_vector(1 downto 0);
   signal SEG_SEL : std_logic_vector(4 downto 0);
   signal SEG_DATA : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: top_module PORT MAP (
          clk => clk,
          rst => rst,
          test_case => test_case,
          motor_state => motor_state,
          SEG_SEL => SEG_SEL,
          SEG_DATA => SEG_DATA
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	
		rst <= '0';
		test_case <= "101";
      wait for clk_period*10;
		
      -- insert stimulus here 

      wait;
   end process;

END;
