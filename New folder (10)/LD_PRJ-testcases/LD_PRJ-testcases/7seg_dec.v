module bcdsevenseg(data, SEG_SEL,SEG_DATA);

input [2:0]data;

output [4:0]SEG_SEL;
output [7:0]SEG_DATA;
reg [7:0]SEG_DATA;

always @(data)
 begin
	case(data)
	0:SEG_DATA = 8'b00111111;
	1:SEG_DATA = 8'b00000110;
	2:SEG_DATA = 8'b01011011;
	4:SEG_DATA = 8'b01001111;
	default:SEG_DATA = 8'b01111111;
	endcase
end

assign SEG_SEL=5'b00001;
endmodule
