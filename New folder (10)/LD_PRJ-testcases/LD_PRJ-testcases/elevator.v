

module elevator(clk, reser, button_in, button_out, floor_present, motor, door);
        in clk;
        in reset;
        in [3:1] button_in; // buttons in floors
        in [3:1] button_out; //buttons in elevator cabin
        in [3:1] floor_present; //sensors indicate elevator position
        out [1:0] motor; // 00 stop, 10 up, 11 down, 10 indicates nothing
        out [3:1] door;// 1 open, 0 close
         
		 
		 ////////////////////////////
		 // put your codes here!!!!!!!!!!!
		 
		 ////////////////////////////
		 
end module
