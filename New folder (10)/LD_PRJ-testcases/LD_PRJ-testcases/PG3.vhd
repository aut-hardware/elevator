library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity PG3 is
Port (
        clk : in std_logic;
        rst : in std_logic;
        motor : in std_logic_vector(1 downto 0);
        rst_int : out std_logic;
        presence : out std_logic_vector(2 downto 0);
        button_in : out std_logic_vector(2 downto 0);
        button_out : out std_logic_vector(2 downto 0)
        );
end PG3;

architecture Behavioral of PG3 is
type state is (s0, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s9p, s12);
signal nx_state: state;
signal pr_state: state; 
signal counter : integer := 0;
begin

p_wait : process (clk, rst, nx_state)
begin
    if(rst = '1') then
        nx_state <=s0;
    elsif (rising_edge(clk)) then
     --   pr_state <= nx_state;

    case nx_state is
        when s0 =>
            counter         <= 0;
            rst_int         <= '1';
            presence        <= "001";
            button_in       <= "000";
            button_out      <= "000";
            nx_state        <= s1;
        when s1 =>       
			rst_int     <= '0';
             if counter = 2 then
                nx_state    <= s2;
                counter     <= 0;
                
            else
                nx_state    <= s1;
                counter     <= counter + 1;
            end if;
        when s2 =>
            button_in       <="100";
            nx_state        <= s3;
        when s3 =>
            if motor = ("10") then 
                presence    <= "000";
                nx_state    <= s4;
            else
                nx_state    <= s3;
            end if;
        when s4 =>
           if counter = 4 then
                nx_state    <= s5;
                counter     <= 0;
            else
                nx_state    <= s4;
                counter     <= counter + 1;
            end if; 
        when s5 =>
            presence        <= "010";
            nx_state        <= s6;            
        when s6 =>
            button_out      <= "010";
            -- presence        <= "000";
            -- nx_state        <= s7;
            if motor = ("10") then 
                presence    <= "000";
                nx_state    <= s7;
            else
                nx_state    <= s6;
            end if;     
        when s7 =>
           if counter = 4 then
                nx_state    <= s8;
                counter     <= 0;
            else
                nx_state    <= s7;
                counter     <= counter + 1;
            end if;  
        when s8 =>
            presence        <= "100";
            
            nx_state        <= s9;  
		  when s9 =>
				button_in       <="000";
				nx_state <= s9p;
        when s9p =>
            if motor = ("11") then 
                presence    <= "000";
                nx_state    <= s10;
            else
                nx_state    <= s9;
            end if;
        when s10 =>
           if counter = 4 then
                nx_state    <= s11;
                counter     <= 0;
            else
                nx_state    <= s10;
                counter     <= counter + 1;
            end if; 
        when s11 =>
            presence        <= "010";
				nx_state        <= s12; 
			when s12 =>
            button_out       <="000";
            nx_state        <= s12;           
    end case;
	 end if;
end process;
end Behavioral;
