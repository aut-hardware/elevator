----------------------------------------------------------------------------------

-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity top_module is
  Port (clk : in std_logic;
        rst : in std_logic;
        test_case : in std_logic_vector (2 downto 0);
        motor_state : out std_logic_vector (1 downto 0);
        SEG_SEL : out std_logic_vector (4 downto 0);
        SEG_DATA : out std_logic_vector (7 downto 0));
end top_module;

architecture Behavioral of top_module is
-------------------------------------------------------
signal door_int  : std_logic_vector(2 downto 0);
signal door_int0 : std_logic_vector(2 downto 0);
signal door_int1 : std_logic_vector(2 downto 0);
signal door_int2 : std_logic_vector(2 downto 0);
signal door_int3 : std_logic_vector(2 downto 0);
signal door_int4 : std_logic_vector(2 downto 0);
signal door_int5 : std_logic_vector(2 downto 0);
signal door_int6 : std_logic_vector(2 downto 0);
signal door_int7 : std_logic_vector(2 downto 0);
------------------------------------------------------
signal button_in_int0 : std_logic_vector(2 downto 0);
signal button_in_int1 : std_logic_vector(2 downto 0);
signal button_in_int2 : std_logic_vector(2 downto 0);
signal button_in_int3 : std_logic_vector(2 downto 0);
signal button_in_int4 : std_logic_vector(2 downto 0);
signal button_in_int5 : std_logic_vector(2 downto 0);
signal button_in_int6 : std_logic_vector(2 downto 0);
signal button_in_int7 : std_logic_vector(2 downto 0);
-------------------------------------------------------
signal button_out_int0 : std_logic_vector(2 downto 0);
signal button_out_int1 : std_logic_vector(2 downto 0);
signal button_out_int2 : std_logic_vector(2 downto 0);
signal button_out_int3 : std_logic_vector(2 downto 0);
signal button_out_int4 : std_logic_vector(2 downto 0);
signal button_out_int5 : std_logic_vector(2 downto 0);
signal button_out_int6 : std_logic_vector(2 downto 0);
signal button_out_int7 : std_logic_vector(2 downto 0);
-------------------------------------------------------
signal floor_present_int0 : std_logic_vector(2 downto 0);
signal floor_present_int1 : std_logic_vector(2 downto 0);
signal floor_present_int2 : std_logic_vector(2 downto 0);
signal floor_present_int3 : std_logic_vector(2 downto 0);
signal floor_present_int4 : std_logic_vector(2 downto 0);
signal floor_present_int5 : std_logic_vector(2 downto 0);
signal floor_present_int6 : std_logic_vector(2 downto 0);
signal floor_present_int7 : std_logic_vector(2 downto 0);
---------------------------------------------------------
signal rst_int0 : std_logic;
signal rst_int1 : std_logic;
signal rst_int2 : std_logic;
signal rst_int3 : std_logic;
signal rst_int4 : std_logic;
signal rst_int5 : std_logic;
signal rst_int6 : std_logic;
signal rst_int7 : std_logic;
---------------------------------------------------------
signal motor_state0 : std_logic_vector (1 downto 0);
signal motor_state1 : std_logic_vector (1 downto 0);
signal motor_state2 : std_logic_vector (1 downto 0);
signal motor_state3 : std_logic_vector (1 downto 0);
signal motor_state4 : std_logic_vector (1 downto 0);
signal motor_state5 : std_logic_vector (1 downto 0);
signal motor_state6 : std_logic_vector (1 downto 0);
signal motor_state7 : std_logic_vector (1 downto 0);
---------------------------------------------------------
signal clk_int : std_logic;


component bcdsevenseg is
    port(
        data : in std_logic_vector(2 downto 0);
        SEG_SEL : out std_logic_vector (4 downto 0);
        SEG_DATA : out std_logic_vector (7 downto 0)
        );
end component;

component elevator is
    port(	 
	
        CLK : in std_logic;
        Reset: in std_logic;
		  InReq: in std_logic_vector (3 downto 1);
		  OutReq: in std_logic_vector (3 downto 1);
		  Present: in std_logic_vector (3 downto 1);
		  Eng : out std_logic_vector( 1 downto 0);
		  Door : out std_logic_vector(3 downto 1)
		  
        );
end component;

begin

DUTT0: entity work.PG0 port map (clk => clk_int, rst => rst, motor => motor_state0, rst_int => rst_int0, presence =>floor_present_int0, 
            button_in => button_in_int0, button_out => button_out_int0);
DUT0: elevator Port map(CLK => clk_int, Reset =>rst_int0, InReq => button_in_int0, OutReq => button_out_int0,
            Present => floor_present_int0, Eng => motor_state0, Door => door_int0);
 --------------------------------------------------------------------------------------------------------------------------------------------       
DUTT1: entity work.PG1 port map (clk => clk_int, rst => rst, motor => motor_state1, rst_int => rst_int1, presence =>floor_present_int1, 
            button_in => button_in_int1, button_out => button_out_int1);
DUT1: elevator Port map(CLK => clk_int, Reset =>rst_int1, InReq => button_in_int1, OutReq => button_out_int1,
            Present => floor_present_int1, Eng => motor_state1, Door => door_int1);
--------------------------------------------------------------------------------------------------------------------------------------------
DUTT2: entity work.PG2 port map (clk => clk_int, rst => rst, motor => motor_state2, rst_int => rst_int2, presence =>floor_present_int2, 
            button_in => button_in_int2, button_out => button_out_int2);
DUT2: elevator Port map(CLK => clk_int, Reset =>rst_int2, InReq => button_in_int2, OutReq => button_out_int2,
            Present => floor_present_int2, Eng => motor_state2, Door => door_int2);
--------------------------------------------------------------------------------------------------------------------------------------------
DUTT3: entity work.PG3 port map (clk => clk_int, rst => rst, motor => motor_state3, rst_int => rst_int3, presence =>floor_present_int3, 
            button_in => button_in_int3, button_out => button_out_int3);
DUT3: elevator Port map(CLK => clk_int, Reset =>rst_int3, InReq => button_in_int3, OutReq => button_out_int3,
            Present => floor_present_int3, Eng => motor_state3, Door => door_int3);
--------------------------------------------------------------------------------------------------------------------------------------------
DUTT4: entity work.PG4 port map (clk => clk_int, rst => rst, motor => motor_state4, rst_int => rst_int4, presence =>floor_present_int4, 
            button_in => button_in_int4, button_out => button_out_int4);
DUT4: elevator Port map(CLK => clk_int, Reset =>rst_int4, InReq => button_in_int4, OutReq => button_out_int4,
            Present => floor_present_int4, Eng => motor_state4, Door => door_int4);
--------------------------------------------------------------------------------------------------------------------------------------------
DUTT5: entity work.PG5 port map (clk => clk_int, rst => rst, motor => motor_state5, rst_int => rst_int5, presence =>floor_present_int5, 
            button_in => button_in_int5, button_out => button_out_int5);
DUT5: elevator Port map(CLK => clk_int, Reset =>rst_int5, InReq => button_in_int5, OutReq => button_out_int5,
            Present => floor_present_int5, Eng => motor_state5, Door => door_int5);
--------------------------------------------------------------------------------------------------------------------------------------------
DUTT6: entity work.PG4 port map (clk => clk_int, rst => rst, motor => motor_state6, rst_int => rst_int6, presence =>floor_present_int6, 
            button_in => button_in_int6, button_out => button_out_int6);
DUT6: elevator Port map(CLK => clk_int, Reset =>rst_int6, InReq => button_in_int6, OutReq => button_out_int6,
            Present => floor_present_int6, Eng => motor_state6, Door => door_int6);
--------------------------------------------------------------------------------------------------------------------------------------------
DUTT7: entity work.PG4 port map (clk => clk_int, rst => rst, motor => motor_state7, rst_int => rst_int7, presence =>floor_present_int7, 
            button_in => button_in_int7, button_out => button_out_int7);
DUT7: elevator Port map(CLK => clk_int, Reset =>rst_int7, InReq => button_in_int7, OutReq => button_out_int7,
            Present => floor_present_int7, Eng => motor_state7, Door => door_int7);

DUT_BCD: bcdsevenseg port map(data => door_int, SEG_SEL => SEG_SEL ,SEG_DATA => SEG_DATA);
DUT_FRE: entity work.freq_divider port map( in_freq => clk, out_freq => clk_int, reset =>rst);

p1: process (test_case)
    begin
        case test_case is
        when "000" =>
            motor_state <= motor_state0;
            door_int <= door_int0;
        when "001" => 
            motor_state <= motor_state1;
            door_int <= door_int1;
        when "010" => 
            motor_state <= motor_state2;
            door_int <= door_int2;
        when "011" => 
            motor_state <= motor_state3;
            door_int <= door_int3;
        when "100" => 
            motor_state <= motor_state4;
            door_int <= door_int4;
        when "101" => 
            motor_state <= motor_state5;
            door_int <= door_int5;
        when "110" => 
            motor_state <= motor_state6;
            door_int <= door_int6;
        when others => 
            motor_state <= motor_state7;
            door_int <= door_int7;
    end case;
    end process;
end Behavioral;
