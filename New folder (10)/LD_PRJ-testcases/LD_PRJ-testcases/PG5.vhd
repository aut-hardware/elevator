library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity PG5 is
Port (
        clk : in std_logic;
        rst : in std_logic;
        motor : in std_logic_vector(1 downto 0);
        rst_int : out std_logic;
        presence : out std_logic_vector(2 downto 0);
        button_in : out std_logic_vector(2 downto 0);
        button_out : out std_logic_vector(2 downto 0)
        );
end PG5;

architecture Behavioral of PG5 is

type state is (s0, s1, s2, s3, s4, s4p, s5, s6, s7, s8, s8p, s9, s10, s11, s12, s9p, s13);
signal nx_state: state;
signal pr_state: state; 
signal counter : integer := 0;
begin

p_wait : process (clk, rst, nx_state)
begin
    if(rst = '1') then
        nx_state <=s0;
    elsif (rising_edge(clk)) then
     --   pr_state <= nx_state;

    case nx_state is
        when s0 =>
            counter <= 0;
            rst_int <= '1';
            presence <= "001";
            button_in <="000";
            button_out <= "000";
            nx_state <= s1;
        when s1 =>             
             if counter = 2 then
                nx_state <=s2;
                counter <= 0;
                rst_int <= '0';
            else
                nx_state <= s1;
                counter <= counter +1;
            end if;
        when s2 =>
            button_in <="010";
            nx_state <= s3;        

        when s3 =>
            if motor = ("10") then 
                presence <= "000";
                nx_state <=s4;
            else
                nx_state <= s3;
            end if;
        when s4 =>
				if counter = 3 then
                nx_state <=s4p;
                counter <= 0;
            else
                nx_state <= s4;
                counter <= counter +1;
            end if; 
				
        when s4p =>
            button_out <= "100";
				nx_state <= s5;
				
        when s5 =>
            presence <= "010";
            nx_state <= s6;
		when s6 =>
            button_in <="000";  
            nx_state <= s7;
        when s7 =>
            if motor = ("10") then 
                 presence <= "000";
                 nx_state <=s8;
            else
                 nx_state <= s7;
            end if;
        when s8 =>					  
            if counter = 2 then
                 nx_state <=s8p;
                 counter <= 0;
            else
                 nx_state <= s8;
                 counter <= counter +1;
            end if; 
				
        when s8p =>
                 button_in <= "001";
					  nx_state <=s9;
					  
        when s9 =>
            presence <= "100";
				nx_state <=s9p;
			when s9p =>
            button_out <="000";
            nx_state <= s10;
        when s10 =>
            if motor = ("11") then 
                 presence <= "000";
                 nx_state <=s11;
            else
                 nx_state <= s10;
            end if;            
        when s11 =>
            if counter = 4 then
                nx_state <=s12;
                counter <= 0;
            else
                nx_state <= s11;
                counter <= counter +1;
            end if;
        when s12 =>        
            presence <= "001";
				nx_state <= s13;
			when s13 =>
            button_in <="000";  
            nx_state <= s13;
        end case;
		  end if;
end process;
end Behavioral;
