`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    09:20:56 12/27/2018 
// Design Name: 
// Module Name:    AdminUser 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

//module AdminUser(num,CLK,RST,Alarm,Admin,Removed,Unlock,Add,Update,current,username);
module AdminUser(num,CLK,RST,Alarm,Unlock);

input [0:3] num;
input CLK;
input RST;
output Alarm;
//output Admin;
//output Removed;
output Unlock;
//output Add;
//output Update;
//output [0:5]current;
//output [11:0]username;


reg Alarm;
reg Admin;
reg Removed;
reg Unlock;
reg Add;
reg Update;




parameter [0:5] Start=0,alarm=1,
				s1=2, s2=3, s3=4, s4=5, CheckUser1=6,
				s5=7, s5_1 =44 , s5_2 = 45 ,s6=8 , s7=9 , s8 = 10 , s9 = 11 , s10 = 12, s11 = 13 , s12 = 14, CheckPass = 15,
				AddCounter = 16,ZeroCounter= 17, UnlockMoving =18, CheckLock = 19,
				s13 = 20 , s14 = 21 , s15 = 22 , s16 = 23 , s17 = 24 , CheckAdmin = 25,
				s18 = 26, s19 = 27, s20 = 28, s21 = 29, s22 = 30, s23 = 31, CheckUser2 = 32, Remove = 33,
				s24 = 34, s25 = 35,s26 = 36,s27 =37, s28 = 38, s29 = 39,CheckUser3 = 40,UnlockAdd=41 ,UpdateAdmin = 42 ,RemoveAdmin = 43 ;
				
parameter [0:3] d0=0 , d1=1 , d2=2, d3=3 , d4=4 , d5=5 , d6=6 , d7=7, d8=8 , d9=9 , star=10 , sharp=11;  
				
				
reg [0:5] current,next;



reg [11:0] username = 12'b000000000000;
reg [11:0] AdminUser;
reg [15:0] password;
reg [3:0] counter;
reg pass_rw=0,admin_rw=0,lock_rw=0,count_rw=0;
reg admin_in,lock_in,cs=1'b0;
wire [15:0] pass_out;
wire [3:0] count_out;
wire admin_out,lock_out;


SYNCSRAM Ram(.clk(CLK),.rst(RST),.cs(cs),
				.pass_rw(pass_rw),.admin_rw(admin_rw),.lock_rw(lock_rw),.count_rw(count_rw),
				.addr(username),.pass_in(password),.count_in(counter),.admin_in(admin_in),.lock_in(lock_in),
				.pass_out(pass_out),.count_out(count_out),.admin_out(admin_out),.lock_out(lock_out));

	
	always @(posedge CLK)
		begin
			if(RST)current <= Start;
			else current <= next;
		end
	
				
	always @(num or current)
		case(current)
			Start:
				begin
					Alarm=1'b0; Admin=1'b0; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					pass_rw=0;admin_rw=0;lock_rw=0;count_rw=0;cs=0;
					if(num==star) next<=s1;
					else next<=Start;
				end
				
			alarm:
				begin
					Alarm=1'b1; Admin=1'b0; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					pass_rw=0;admin_rw=0;lock_rw=0;count_rw=0;cs=0;
					next<=Start;
				end
			s1:
				begin
					Alarm=1'b0; Admin=1'b0; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					if(4'b0000 <= num & num <= 4'b1001) 
						begin
							username[11:8]<= num;
							next<=s2;
						end
					else next<=alarm;
				end
			s2:
				begin
					Alarm=1'b0; Admin=1'b0; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
				
					if(4'b0000 <= num & num <= 4'b1001) 
						begin
							username[7:4]<= num;
							next<=s3;
						end
					else next<=alarm;
				end
			s3:
				begin
					Alarm=1'b0; Admin=1'b0; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
				
					if(4'b0000 <= num & num <= 4'b1001) 
						begin
							username[3:0]<= num;
							next<=s4;
						end
					else next<=alarm;
				end
			s4:
				begin
					Alarm=1'b0; Admin=1'b0; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					
					if(12'b000000000001 <= username & username <= 12'b000100101000 & num == star)
						begin
							cs=1;
							next<=CheckUser1;
						end
					else
						next<=alarm;
				end
			CheckUser1:
				begin
					Alarm=1'b0; Admin=1'b0; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
						if (lock_out == 0)
							next <= s5;
						else  next <= alarm;
				end
			s5:
				begin
					Alarm=1'b0; Admin=1'b0; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0;
						if(admin_out) next <=s5_1;
						else next<=s5_2;
				end
			s5_1:
				begin
					Alarm=1'b0; Admin=1'b1; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					if(4'b0000 <= num & num <= 4'b1001) 
						begin
							password[15:12]<= num;
							next<=s13;
						end
					else next<=alarm;
				end
			s5_2:
				begin
					Alarm=1'b0; Admin=1'b0; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					if(num==sharp) next<=s6;
					else next<=alarm;
				end
			s6:
				begin
					Alarm=1'b0; Admin=1'b0; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					if(num == star) next<=s7;
					else next<=alarm;
				end
			s7:
				begin
					Alarm=1'b0; Admin=1'b0; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					if(4'b0000 <= num & num <= 4'b1001) 
						begin
							password[15:12]<= num;
							next<=s8;
						end
					else next<=alarm;
				end
			s8:
				begin
					Alarm=1'b0; Admin=1'b0; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					if(4'b0000 <= num & num <= 4'b1001) 
						begin
							password[11:8]<= num;
							next<=s9;
						end
					else next<=alarm;
				end
			s9:
				begin
					Alarm=1'b0; Admin=1'b0; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					if(4'b0000 <= num & num <= 4'b1001) 
						begin
							password[7:4]<= num;
							next<=s10;
						end
					else next<=alarm;
				end
			s10:
				begin
					Alarm=1'b0; Admin=1'b0; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					if(4'b0000 <= num & num <= 4'b1001) 
						begin
							password[3:0]<= num;
							next<=s11;
						end
					else next<=alarm;
				end
			s11:
				begin
					Alarm=1'b0; Admin=1'b0; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					if(num==star) next<=s12;
					else next<=alarm;
				end
			s12:
				begin
					Alarm=1'b0; Admin=1'b0; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					if(num==sharp) next<=CheckPass;
					else next<=alarm;
				end
			CheckPass:
				begin
					Alarm=1'b0; Admin=1'b0; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					if(pass_out == password)
						next <= ZeroCounter;
					else next <= AddCounter;
				end
			AddCounter:
				begin
					Alarm=1'b0; Admin=1'b0; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					count_rw = 1;
					counter = count_out + 1'b1;
					next <= CheckLock;
				end
			ZeroCounter:
				begin
					Alarm=1'b0; Admin=1'b0; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					count_rw = 1;
					counter = 0;
					next <= UnlockMoving;
				end
			UnlockMoving:// Must be attach to moving part
				begin
					Alarm=1'b0; Admin=1'b0; Removed=1'b0; Unlock=1'b1; Add=1'b0; Update=1'b0; 
					pass_rw=0;admin_rw=0;lock_rw=0;count_rw=0;
					next <= Start;
					
				end
			CheckLock:
				begin
					Alarm=1'b0; Admin=1'b0; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					pass_rw=0;admin_rw=0;lock_rw=0;count_rw=0;
					if(counter == 4'b0011)
						begin
							lock_rw = 1;
							lock_in = 1;
						end
					next <= alarm;
					
				end
			s13:
				begin
					Alarm=1'b0; Admin=1'b1; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					if(4'b0000 <= num & num <= 4'b1001) 
						begin
							password[11:8]<= num;
							next<=s14;
						end
					else next<=alarm;				
				end
			s14:
				begin
					Alarm=1'b0; Admin=1'b1; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					if(4'b0000 <= num & num <= 4'b1001) 
						begin
							password[7:4]<= num;
							next<=s15;
						end
					else next<=alarm;
				end
			s15:
				begin
					Alarm=1'b0; Admin=1'b1; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					if(4'b0000 <= num & num <= 4'b1001) 
						begin
							password[3:0]<= num;
							next<=s16;
						end
					else next<=alarm;
				end
			s16:
				begin
					Alarm=1'b0; Admin=1'b1; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					if(num == star) 
						begin
							next<=s17;
						end
					else next<=alarm;
				end
			s17:
				begin
					Alarm=1'b0; Admin=1'b1; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					if(num == sharp) next<=CheckAdmin;
					else next <= alarm;
				end
			CheckAdmin:
				begin
					Alarm=1'b0; Admin=1'b1; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
						if(admin_out & password == pass_out) 
							begin
								AdminUser = username;
								next<=s18;
							end
						else next<= alarm;
				end
			s18:
				begin
					Alarm=1'b0; Admin=1'b1; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					if(num==star) 
						begin
							cs = 0;
							next<=s19;
						end
					else next<=alarm;
				end
			s19:
				begin
					Alarm=1'b0; Admin=1'b1; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					if(4'b0000 <= num & num <= 4'b1001) 
						begin
							username[11:8]<= num;
							next<=s20;
						end
					else next<=alarm;				
				end
			s20:
				begin
					Alarm=1'b0; Admin=1'b1; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					if(4'b0000 <= num & num <= 4'b1001) 
						begin
							username[7:4]<= num;
							next<=s21;
						end
					else next<=alarm;
				end
			s21:
				begin
					Alarm=1'b0; Admin=1'b1; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					if(4'b0000 <= num & num <= 4'b1001) 
						begin
							username[3:0]<= num;
							next<=s22;
						end
					else next<=alarm;
				end
			s22:
				begin
					Alarm=1'b0; Admin=1'b1; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					if(num == star) next<=s23;
					else if(num==sharp) next<=s24;
					else next<=alarm;
				end
			s23:
				begin
					Alarm=1'b0; Admin=1'b1; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					if( 12'b000000000001 <= username & username <= 12'b000100101000 & num==sharp) 
						begin
							cs = 1;
							next<=CheckUser2;
						end
					else next<=alarm;
				end
			CheckUser2: 
				begin
					Alarm=1'b0; Admin=1'b1; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					if(lock_out == 0) next <= Remove;
					else next <= alarm;
				end
			Remove:
				begin
					Alarm=1'b0; Admin=1'b1; Removed=1'b1; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					lock_rw = 1;
					lock_in = 1;
					next <= Start;
				end
			s24:
				begin
					Alarm=1'b0; Admin=1'b1; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					if(4'b0000 <= num & num <= 4'b1001) 
						begin
							password[15:12]<= num;
							next<=s25;
						end
					else if(num==sharp) next<= UpdateAdmin;
					else next<=alarm;
				end
			s25:
				begin
					Alarm=1'b0; Admin=1'b1; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					if(4'b0000 <= num & num <= 4'b1001) 
						begin
							password[11:8]<= num;
							next<=s26;
						end
					else next<=alarm;
				end
			s26:
				begin
					Alarm=1'b0; Admin=1'b1; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					if(4'b0000 <= num & num <= 4'b1001) 
						begin
							password[7:4]<= num;
							next<=s27;
						end
					else next<=alarm;
				end
			s27:
				begin
					Alarm=1'b0; Admin=1'b1; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					if(4'b0000 <= num & num <= 4'b1001) 
						begin
							password[3:0]<= num;
							next<=s28;
						end
					else next<=alarm;
				end
			s28:
				begin
					Alarm=1'b0; Admin=1'b1; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					if(num == star) next<=s29;
					else next<=alarm;
				end
			s29:
				begin
					Alarm=1'b0; Admin=1'b1; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					if( 12'b000000000001 <= username & username <= 12'b000100101000 & num==sharp) 
						begin
							cs = 1;
							next<=CheckUser3;
						end
					else next<=alarm;
				end
			CheckUser3:
				begin
					Alarm=1'b0; Admin=1'b1; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b0; 
					if(lock_out == 0) next <= alarm;
					else next<= UnlockAdd;
				end
			UnlockAdd:
				begin
					Alarm=1'b0; Admin=1'b1; Removed=1'b0; Unlock=1'b0; Add=1'b1; Update=1'b0; 
					lock_rw = 1; count_rw = 1; pass_rw = 1;
					counter = 4'b0000; lock_in = 0;
					next <= Start;
				end
			UpdateAdmin:
				begin
					Alarm=1'b0; Admin=1'b1; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b1; 
					cs=1;
					admin_rw = 1;
					admin_in = 1;
					next <= RemoveAdmin;
				end
			RemoveAdmin:
				begin
					Alarm=1'b0; Admin=1'b1; Removed=1'b0; Unlock=1'b0; Add=1'b0; Update=1'b1; 
					username = AdminUser;
					admin_rw = 1;
					admin_in = 0;
					next <= Start;
				end

		endcase
				
				
				
				
				
				
				
				
				


endmodule
