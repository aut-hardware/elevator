`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    08:24:56 01/02/2019 
// Design Name: 
// Module Name:    TB 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module TB(
    );
reg CLK,RST;
reg [3:0] num;
wire Alarm,Unlock;
//wire Alarm,Admin,Removed,Unlock,Add,Update;

//wire [0:5]current;
//wire [11:0] username;


//AdminUser DUT (num,CLK,RST,Alarm,Admin,Removed,Unlock,Add,Update,current,username);
AdminUser DUT (num,CLK,RST,Alarm,Unlock);


parameter [0:3] d0=0 , d1=1 , d2=2, d3=3 , d4=4 , d5=5 , d6=6 , d7=7, d8=8 , d9=9 , star=10 , sharp=11;  
				

initial
	begin
		CLK = 1'b0;
		#5;
		repeat(1000)
			begin
				CLK = ~ CLK;
				#10;
			end
	end

initial
	begin
		RST = 1'b1;
		#20;
		RST = 1'b0;
	end

initial
	begin
		
		#20;
		//Restart   Admin : 001
		num = star;
		
		#20
		num = d0;
		
		#20
		num = d0;
		
		#20
		num = d1;
		
		#20
		num = star;
		
		#60
		num = d1;
		
		#20
		num = d1;
		
		#20
		num = d1;
		
		#20
		num = d1;
		
		#20
		num = star;
		
		#20
		num = sharp;
		
		//User 001 enter to Admin
		
		#40
		num = star;
		
		#20
		num = d0;
		
		#20
		num = d0;
		
		#20
		num = d2;
		
		#20
		num = sharp;
		
		#20
		num = d1;
		
		#20
		num = d2;
		
		#20
		num = d3;
		
		#20
		num = d4;
		
		#20
		num = star;
		
		#20
		num = sharp;
		
		// User 002 Aded
		#80
		num = star;
		
		#20
		num = d0;
		
		#20
		num = d0;
		
		#20
		num = d2;
		
		#20
		num = star;
		
		#60
		num = sharp;
		
		#20
		num = star;
		
		#20
		num = d1;
		
		#20
		num = d2;
		
		#20
		num = d3;
		
		#20
		num = d4;
		
		#20
		num = star;
		
		#20
		num = sharp;
		
		//User 002 enter to Moving
		
		
		#80
		num = star;
		
		#20
		num = d0;
		
		#20
		num = d0;
		
		#20
		num = d1;
		
		#20
		num = star;
		
		#60
		num = d1;
		
		#20
		num = d1;
		
		#20
		num = d1;
		
		#20
		num = d1;
		
		#20
		num = star;
		
		#20
		num = sharp;
		
		#40
		num = star;
		
		#20
		num = d0;
		
		#20
		num = d0;
		
		#20
		num = d3;
		
		#20
		num = star;
		
		#20
		num = sharp;
		
		//Admin try to removed incorrect user 003
		
		#80
		num = star;
		
		#20
		num = d0;
		
		#20
		num = d0;
		
		#20
		num = d1;
		
		#20
		num = star;
		
		#60
		num = d1;
		
		#20
		num = d1;
		
		#20
		num = d1;
		
		#20
		num = d1;
		
		#20
		num = star;
		
		#20
		num = sharp;
		
		#40
		num = star;
		
		#20
		num = d0;
		
		#20
		num = d0;
		
		#20
		num = d2;
		
		#20
		num = sharp;
		
		#20
		num = sharp;
		
		// Admin changed to 002
		
		#80;
		num = star;
		
		#20
		num = d0;
		
		#20
		num = d0;
		
		#20
		num = d2;
		
		#20
		num = star;
		
		#60
		num = d1;
		
		#20
		num = d2;
		
		#20
		num = d3;
		
		#20
		num = d4;
		
		#20
		num = star;
		
		#20
		num = sharp;
		
		#40
		num = star;
		
		#20
		num = d0;
		
		#20
		num = d0;
		
		#20
		num = d3;
		
		#20
		num = sharp;
		
		#20
		num = d4;
		
		#20
		num = d3;
		
		#20
		num = d2;
		
		#20
		num = d1;
		
		#20
		num = star;
		
		#20
		num = sharp;
		
		// User 003 Aded
		
		//User 001 try to enter Admin 
		#80;
		num = star;
		
		#20
		num = d0;
		
		#20
		num = d0;
		
		#20
		num = d1;
		
		#20
		num = star;
		
		#60
		num = d1;		
		//User 001 not allowed


		#80
		num = star;
		
		#20
		num = d0;
		
		#20
		num = d0;
		
		#20
		num = d2;
		
		#20
		num = star;
		
		#60
		num = d1;
		
		#20
		num = d2;
		
		#20
		num = d3;
		
		#20
		num = d4;
		
		#20
		num = star;
		
		#20
		num = sharp;
		
		#40
		num = star;
		
		#20
		num = d0;
		
		#20
		num = d0;
		
		#20
		num = d3;
		
		#20
		num = star;
		
		#20
		num = sharp;
		//Admin removed user 003 
		
		#80
		num = star;
		
		#20
		num = d0;
		
		#20
		num = d0;
		
		#20
		num = d4;
		
		#20
		num = star;
		
		//User 004 that is not Exist try to enter ... Unsuccessful
		
		#60
		num = star;
		
		#20
		num = d0;
		
		#20
		num = d0;
		
		#20
		num = d3;
		
		#20
		num = star;
		//User 003 that is removed try to enter ... Unsuccessful
		#60;
		num = star;
		
		#20
		num = d0;
		
		#20
		num = d0;
		
		#20
		num = d1;
		
		#20
		num = star;
		
		#60
		num = sharp;
		
		#20
		num = star;
		
		#20
		num = d7;
		
		#20
		num = d8;
		
		#20
		num = d9;
		
		#20
		num = d0;
		
		#20
		num = star;
		
		#20
		num = sharp;
		
		// User 001 incorrect:1
		#100
		num = star;
		
		#20
		num = d0;
		
		#20
		num = d0;
		
		#20
		num = d1;
		
		#20
		num = star;
		
		#60
		num = sharp;
		
		#20
		num = star;
		
		#20
		num = d7;
		
		#20
		num = d8;
		
		#20
		num = d9;
		
		#20
		num = d0;
		
		#20
		num = star;
		
		#20
		num = sharp;
		
		// User 001 incorrect:2
		#100
		num = star;
		
		#20
		num = d0;
		
		#20
		num = d0;
		
		#20
		num = d1;
		
		#20
		num = star;
		
		#60
		num = sharp;
		
		#20
		num = star;
		
		#20
		num = d1;
		
		#20
		num = d1;
		
		#20
		num = d1;
		
		#20
		num = d1;
		
		#20
		num = star;
		
		#20
		num = sharp;
				
		// User 001 incorrect:0 
		#100
		num = star;
		
		#20
		num = d0;
		
		#20
		num = d0;
		
		#20
		num = d1;
		
		#20
		num = star;
		
		#60
		num = sharp;
		
		#20
		num = star;
		
		#20
		num = d7;
		
		#20
		num = d8;
		
		#20
		num = d9;
		
		#20
		num = d0;
		
		#20
		num = star;
		
		#20
		num = sharp;
		
		// User 001 incorrect:1

		#100
		num = star;
		
		#20
		num = d0;
		
		#20
		num = d0;
		
		#20
		num = d1;
		
		#20
		num = star;
		
		#60
		num = sharp;
		
		#20
		num = star;
		
		#20
		num = d7;
		
		#20
		num = d8;
		
		#20
		num = d9;
		
		#20
		num = d0;
		
		#20
		num = star;
		
		#20
		num = sharp;
		
		// User 001 incorrect:2
		#100
		num = star;
		
		#20
		num = d0;
		
		#20
		num = d0;
		
		#20
		num = d1;
		
		#20
		num = star;
		
		#60
		num = sharp;
		
		#20
		num = star;
		
		#20
		num = d7;
		
		#20
		num = d8;
		
		#20
		num = d9;
		
		#20
		num = d0;
		
		#20
		num = star;
		
		#20
		num = sharp;
		
		// User 001 incorrect:3 
		// User 001 LOCKED !

		#100
		num = star;
		
		#20
		num = d0;
		
		#20
		num = d0;
		
		#20
		num = d1;
		
		#20
		num = star;
		
		// User 001 not allowed
		#60
		num = star;
		
		#20
		num = d0;
		
		#20
		num = d0;
		
		#20
		num = d2;
		
		#20
		num = star;
		
		#60
		num = d1;
		
		#20
		num = d2;
		
		#20
		num = d3;
		
		#20
		num = d4;
		
		#20
		num = star;
		
		#20
		num = sharp;
		
		#40
		num = star;
		
		#20
		num = d0;
		
		#20
		num = d0;
		
		#20
		num = d1;
		
		#20
		num = sharp;
		
		#20
		num = d2;
		
		#20
		num = d2;
		
		#20
		num = d2;
		
		#20
		num = d2;
		
		#20
		num = star;
		
		#20
		num = sharp;
		
		//Admin unlocked user 001
		
		#60
		num = star;
		
		#20
		num = d0;
		
		#20
		num = d0;
		
		#20
		num = d1;
		
		#20
		num = star;
		
		#60
		num = sharp;
		
		#20
		num = star;
		
		#20
		num = d2;
		
		#20
		num = d2;
		
		#20
		num = d2;
		
		#20
		num = d2;
		
		#20
		num = star;
		
		#20
		num = sharp;
		//User 001 enter to moving with new Password
		
		#100
		$finish;
		
		// FINISHED :)
		
	end



endmodule
